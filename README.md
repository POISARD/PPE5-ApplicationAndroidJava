# PPE5 - Application Android Java

L'application est accompagnée de toute sa documentation ainsi que son sujet. Les répertoires sont :

+ **Documentation**

   La documentation comporte : Le sujet donnant les objectifs et contraintes, puis la documentation rédigée par le groupe à propos du projet.

+ **Prod**
  * androidM2L (dossier des sources pour développer sous Eclipse Android SDK)
  * APK (dossier comportant l'APK de l'application)
  * Les autres dossiers sont crées lors du développement sous Eclipse

   Les productions sont l'APK pour lancer l'application depuis un appareil Android, et les sources afin de pouvoir modifier le code du projet sous Eclipse.

+ **Ressources**

Les ressources contiennent les images utilisées lors du projet et la clé crée lors de l'export de l'application de ses sources au format APK.

## Exploitation - déploiement

L'application sous sa forme d'APK est directement exploitable en tant qu'application tierce (n'étant pas du store officiel, l'android doit être configuré de sorte à accepter les applications non officielles et dont les sources ne sont pas vérifiées), et la version d'android nécessaire au fonctionnement est 4.1.X (soit l'android Jelly Bean).

Les sources elles sont exploitables sous Eclipse avec le SDK Android (niveau de l'API : 16 - 19).

## Contexte

L'entreprise M2L souhaite pour ses clients un accès mobile à la consultation de cours et conférences. Une application web réalisée pendant l'année (Sous le micro framework Silex) donne l'accès à un web-service permettant l'envoi de .json contenant les cours et conférences de la base de données actuelle de l'application Silex.

Ainsi, l'application android ne demande pas de mise à jour car les .json suivront les évolutions de l'application Silex. Une application est donc demandée, permettant la consultation selon deux catégories.
package metier;

public class Conference {
	private int idConf;
	private String intituleConf;
	private String descriptionConf;
	private Intervenant idIntervenant;
	private Categorie idCategorie;
	
	public Conference(int idConf, String intituleConf, String descriptionConf, Intervenant idIntervenant, Categorie idCategorie) {
		super();
		this.idConf = idConf;
		this.intituleConf = intituleConf;
		this.descriptionConf = descriptionConf;
		this.idIntervenant = idIntervenant;
		this.idCategorie = idCategorie;
	}
	
	public int getIdConf() {
		return idConf;
	}
	public void setIdConf(int idConf) {
		this.idConf = idConf;
	}
	public String getIntituleConf() {
		return intituleConf;
	}
	public void setIntituleConf(String intituleConf) {
		this.intituleConf = intituleConf;
	}
	public String getDescriptionConf() {
		return descriptionConf;
	}
	public void setDescriptionConf(String descriptionConf) {
		this.descriptionConf = descriptionConf;
	}

	public String toString() {
		return "Conference [idConf=" + idConf + ", intituleConf=" + intituleConf + ", descriptionConf="
				+ descriptionConf + ", idIntervenant=" + idIntervenant + ", idCategorie=" + idCategorie + "]";
	}

}
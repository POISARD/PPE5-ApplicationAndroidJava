package metier;

public class Categorie {
	private int idCat;
	private String libelle;

	public Categorie(int idCat, String libelle) {
		super();
		this.idCat = idCat;
		this.libelle = libelle;
	}
	public int getIdCat() {
		return idCat;
	}
	public void setIdCat(int idCat) {
		this.idCat = idCat;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	
	public String toString() {
		return "Categorie [idCat=" + idCat + ", libelle=" + libelle + "]";
	}
}

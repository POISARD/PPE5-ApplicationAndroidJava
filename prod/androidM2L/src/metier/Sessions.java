package metier;

public class Sessions {
	private int idSession;
	private String horairedebut;
	private String horairefin;
	private Cours id;
	private Intervenant idIntervenant;
	
	public Sessions(int idSession, String horairedebut, String horairefin, Cours id, Intervenant idIntervenant) {
		super();
		this.idSession = idSession;
		this.horairedebut = horairedebut;
		this.horairefin = horairefin;
		this.id = id;
		this.idIntervenant = idIntervenant;
	}
	public int getIdSession() {
		return idSession;
	}
	public void setIdSession(int idSession) {
		this.idSession = idSession;
	}
	public String getHorairedebut() {
		return horairedebut;
	}
	public void setHorairedebut(String horairedebut) {
		this.horairedebut = horairedebut;
	}
	public String getHorairefin() {
		return horairefin;
	}
	public void setHorairefin(String horairefin) {
		this.horairefin = horairefin;
	}
	public Cours getId() {
		return id;
	}
	public void setId(Cours id) {
		this.id = id;
	}
	public Intervenant getIdIntervenant() {
		return idIntervenant;
	}
	public void setIdIntervenant(Intervenant idIntervenant) {
		this.idIntervenant = idIntervenant;
	}

	public String toString() {
		return "Sessions [idSession=" + idSession + ", horairedebut=" + horairedebut + ", horairefin=" + horairefin
				+ ", id=" + id + ", idIntervenant=" + idIntervenant + "]";
	}
}
package metier;

import java.util.ArrayList;

public class Cours {

	private int idCours;
	private String intituleCours;
	private String DescriptionCours;
	private Categorie idCategorie;
	private ArrayList<Sessions> LesSessions;

	public Cours(int idCours, String intituleCours, String descriptionCours, Categorie idCategorie) {
		super();
		this.idCours = idCours;
		this.intituleCours = intituleCours;
		this.DescriptionCours = descriptionCours;
		this.idCategorie = idCategorie;
	}
	public ArrayList<Sessions> getLesSessions() {
		return LesSessions;
	}
	public void setLesSessions(Sessions lesSessions) {
		this.LesSessions.add(lesSessions);
	}
	public int getIdCours() {
		return idCours;
	}
	public void setIdCours(int idCours) {
		this.idCours = idCours;
	}
	public String getIntituleCours() {
		return intituleCours;
	}
	public void setIntituleCours(String intituleCours) {
		this.intituleCours = intituleCours;
	}
	public String getDescriptionCours() {
		return DescriptionCours;
	}
	public void setDescriptionCours(String descriptionCours) {
		DescriptionCours = descriptionCours;
	}

	public String toString() {
		return "Cours [idCours=" + idCours + ", intituleCours=" + intituleCours + ", DescriptionCours="
				+ DescriptionCours + ", id Categorie=" + idCategorie;
	}
}

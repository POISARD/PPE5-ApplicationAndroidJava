package metier;

public class Intervenant {
	private int idIntervenant;
	private String nomIntervenant;
	public Intervenant(int idIntervenant, String nomIntervenant) {
		super();
		this.idIntervenant = idIntervenant;
		this.nomIntervenant = nomIntervenant;
	}
	
	public int getIdIntervenant() {
		return idIntervenant;
	}
	public void setIdIntervenant(int idIntervenant) {
		this.idIntervenant = idIntervenant;
	}
	public String getNomIntervenant() {
		return nomIntervenant;
	}
	public void setNomIntervenant(String nomIntervenant) {
		this.nomIntervenant = nomIntervenant;
	}

	public String toString() {
		return "Intervenant [idIntervenant=" + idIntervenant + ", nomIntervenant=" + nomIntervenant + "]";
	}
}

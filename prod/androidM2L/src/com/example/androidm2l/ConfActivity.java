package com.example.androidm2l;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ConfActivity extends Activity  {

	ListView maListView;
	String resultat;
	int positionListe;
	List<String> myListofData = new ArrayList<String>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_conf);
		maListView = (ListView)findViewById(R.id.listView1);
		resultat = this.getIntent().getExtras().getString("donnees");
		try{
			JSONArray dataValues = new JSONArray(resultat);
			for(int j=0; j<dataValues.length(); j++){
				JSONObject values = dataValues.getJSONObject(j);
				String valCol1 = values.getString("idConf");
				String valCol2 = values.getString("intituleConf");
				String valCol3 = values.getString("descriptionConf");
				myListofData.add(valCol1 + " - " + valCol2 + "\n\n Description : \n" + valCol3 +"\n ");
				maListView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,myListofData));
			}
			maListView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, myListofData));
		}
		catch(Exception e){
			Log.i("m2l_android", "" +e.getMessage());
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.conf, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}

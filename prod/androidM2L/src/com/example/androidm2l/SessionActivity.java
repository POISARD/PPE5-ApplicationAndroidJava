package com.example.androidm2l;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class SessionActivity extends Activity {

	ListView maListView;
	String resultat;
	String position;
	String resultatSession;
	List<String> myListofData = new ArrayList<String>();
	TextView titreSession;
	TextView interSession;
	TextView horairesSession;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_session);
		titreSession = (TextView) findViewById(R.id.textView2);
		interSession = (TextView) findViewById(R.id.textView3);
		horairesSession = (TextView) findViewById(R.id.textView4);
		maListView = (ListView)findViewById(R.id.listView1);
		position = this.getIntent().getExtras().getString("position");
		resultat = this.getIntent().getExtras().getString("donnee");
		resultatSession = this.getIntent().getExtras().getString("donneeSession");
		try {
			JSONArray dataValues = new JSONArray(resultat);
			for(int j=0; j<dataValues.length(); j++){
				JSONObject values = dataValues.getJSONObject(j);
				Log.i("androidM2L","BUG ou NON : "+values);
				Log.i("androidM2L","Position : "+j);
				if(j == Integer.parseInt(position)){					
					String valCol1 = values.getString("idSessionCours");
					Log.i("androidM2L","valCol1 : "+myListofData);
					titreSession.setText("Session n°"+valCol1);
					String valCol2 = values.getString("horairedebut");
					String valCol3 = values.getString("horairefin");
					horairesSession.setText("Horaires : "+valCol2+" - "+valCol3);
					String valCol4 = values.getString("nom_Intervenant");
					interSession.setText("Intervenant : "+valCol4);
					myListofData.add("Numéro de session : " + valCol1 + " -  Horaire : "+ valCol2 + " - " + valCol3 + " Intervenant :" + valCol4);
					//maListView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,myListofData));
				}				
			}
			Log.i("androidM2L","Liste de la session : "+myListofData);
			maListView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, myListofData));
		}
		catch(Exception e){
		Log.i("m2l_android", "Erreur TryCatch : " +e.getMessage());
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.session, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}

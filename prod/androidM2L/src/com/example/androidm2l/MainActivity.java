package com.example.androidm2l;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity implements OnClickListener {
	
	// Liste des composants
	Button btConf;
	Button btCour;
	String resultatConfs; 
	String resultatCours;
	LongRunningGetIOConf monRunningConfs = new LongRunningGetIOConf();
	LongRunningGetIOCours monRunningCours = new LongRunningGetIOCours();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		// Affectation des composants
		btConf=(Button)findViewById(R.id.button1);
		btCour=(Button)findViewById(R.id.button2);
		// Affectation des Listener
		btConf.setOnClickListener(this);
		btCour.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		// Si le bouton est celui des Conferences
		if(v == btConf){
			Intent intent = new Intent(MainActivity.this,ConfActivity.class);
			try{
				resultatConfs = monRunningConfs.execute().get();
			}catch(Exception e){
				Log.i("m2l_android", e.getLocalizedMessage());
			}
			intent.putExtra("donnees", resultatConfs);
			startActivity(intent);
		}
		// Si le bouton est celui des Cours
		if(v == btCour){
			Intent intent = new Intent(MainActivity.this,CoursActivity.class);
			try{
				resultatCours = monRunningCours.execute().get();
			}catch(Exception e){
				Log.i("m2l_android", e.getLocalizedMessage());
			}
			intent.putExtra("donnees", resultatCours);
			startActivity(intent);
		}
	}
	
	protected class LongRunningGetIOConf extends AsyncTask <Void, Void, String> {
		@Override
		protected String doInBackground(Void... params) {
			List<String> myListofData=new ArrayList<String>();
			HttpClient httpClient = new DefaultHttpClient();
			HttpContext localContext = new BasicHttpContext();
			HttpGet httpGet = new HttpGet("http://cr-devtux16.leschartreux.com/~t.poisard/formaweb/index.php/api/conferences");

			String text = null;
			try {
				HttpResponse response = httpClient.execute(httpGet, localContext);
				HttpEntity entity = response.getEntity();
				text = EntityUtils.toString(entity);
			} catch (Exception e) {
				return e.getLocalizedMessage();
			}
				return text;
			}
		}
		protected void onPostExecute(String results) {
			if (results!=null) {
				//resultat=results;
			}
			Button b = (Button)findViewById(R.id.button1);
			b.setClickable(true);
		}
		
		protected class LongRunningGetIOCours extends AsyncTask <Void, Void, String> {
			@Override
			protected String doInBackground(Void... params) {
				List<String> myListofData=new ArrayList<String>();
				HttpClient httpClient = new DefaultHttpClient();
				HttpContext localContext = new BasicHttpContext();
				HttpGet httpGet = new HttpGet("http://cr-devtux16.leschartreux.com/~c.sirot/formaweb/api/cours");

				String text = null;
				try {
					HttpResponse response = httpClient.execute(httpGet, localContext);
					HttpEntity entity = response.getEntity();
					text = EntityUtils.toString(entity);
				} catch (Exception e) {
					return e.getLocalizedMessage();
				}
					return text;
				}
			}
			protected void onPostExecute2(String results) {
				if (results!=null) {
				//	resultat=results;
				}
				Button b = (Button)findViewById(R.id.button1);
				b.setClickable(true);
			}
}

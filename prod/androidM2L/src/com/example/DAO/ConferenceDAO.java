package com.example.DAO;

import java.util.ArrayList;

import metier.Conference;

public class ConferenceDAO {

static private ArrayList<Conference> lesConf = new ArrayList<Conference>();
	
	private ConferenceDAO() {}

	static public ArrayList<Conference> getLesConf() {
		return lesConf;
	}

	static public void setLesConf(ArrayList<Conference> lesConf) {
		ConferenceDAO.lesConf = lesConf;
	}
	
	static public Conference getUneConf(int position){
		return ConferenceDAO.lesConf.get(position);
	}
}

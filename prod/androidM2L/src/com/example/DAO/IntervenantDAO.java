package com.example.DAO;

import java.util.ArrayList;

import metier.Intervenant;

public class IntervenantDAO {

	static private ArrayList<Intervenant> lesIntervenant = new ArrayList<Intervenant>();
	
	private IntervenantDAO() {}

	static public ArrayList<Intervenant> getLesIntervenants() {
		return lesIntervenant;
	}

	static public void setLesIntervenant(ArrayList<Intervenant> lesIntervenant) {
		IntervenantDAO.lesIntervenant = lesIntervenant;
	}
	
	static public Intervenant getUnIntervenant(int position){
		return IntervenantDAO.lesIntervenant.get(position);
	}
}

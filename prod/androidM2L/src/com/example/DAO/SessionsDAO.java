package com.example.DAO;

import java.util.ArrayList;

import metier.Sessions;

public class SessionsDAO {

	static private ArrayList<Sessions> lesSessionConf = new ArrayList<Sessions>();
	
	private SessionsDAO() {}

	static public ArrayList<Sessions> getLesSessionConfs() {
		return lesSessionConf;
	}

	static public void setLesSessionConf(ArrayList<Sessions> lesSessionConf) {
		SessionsDAO.lesSessionConf = lesSessionConf;
	}
	
	static public Sessions getUnSessionConf(int position){
		return SessionsDAO.lesSessionConf.get(position);
	}
}

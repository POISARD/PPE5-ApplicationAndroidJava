package com.example.DAO;

import java.util.ArrayList;

import metier.Cours;

public class CoursDAO {

	static private ArrayList<Cours> lesCours = new ArrayList<Cours>();
	
	private CoursDAO() {}

	static public ArrayList<Cours> getLesCours() {
		return lesCours;
	}

	static public void setLesCours(ArrayList<Cours> lesCours) {
		CoursDAO.lesCours = lesCours;
	}
	
	static public Cours getUnCours(int position){
		return CoursDAO.lesCours.get(position);
	}
	
}


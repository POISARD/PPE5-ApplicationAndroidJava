package com.example.DAO;

import java.util.ArrayList;

import metier.Categorie;

public class CategorieDAO {
	
	static private ArrayList<Categorie> lesCategories = new ArrayList<Categorie>();
	
	private CategorieDAO() {}

	static public ArrayList<Categorie> getLesCategories() {
		return lesCategories;
	}

	static public void setLesCours(ArrayList<Categorie> lesCategories) {
		CategorieDAO.lesCategories = lesCategories ;
	}
	
	static public Categorie getUnCours(int position){
		return CategorieDAO.lesCategories.get(position);
	}
}